<?php

namespace App\Command\News;

use App\Repository\NewsRepository;
use App\Service\Parser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseCommand extends Command
{
    protected static $defaultName = 'app:parse';
    /**
     * @var Parser
     */
    private $parser;
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    public function __construct(string $name = null, Parser $parser, NewsRepository $newsRepository)
    {
        parent::__construct($name);
        $this->parser = $parser;
        $this->newsRepository = $newsRepository;
    }

    protected function configure()
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->newsRepository->truncate();
            $this->parser->parse();
            return Command::SUCCESS;
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getTraceAsString();
            return Command::FAILURE;
        }
    }
}
