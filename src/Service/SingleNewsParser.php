<?php

namespace App\Service;

use App\Entity\News;
use PHPHtmlParser\Dom;

class SingleNewsParser
{
    public function __construct()
    {
    }

    public function parse(Dom\Node\HtmlNode $htmlNode): ?News
    {
        $dom = new Dom;
        $dom->loadFromUrl($htmlNode->getAttribute('href'));
        $article = $dom->find('.article__text')[0];

        if ($article !== null) {
            $news = new News();
            $parts = $article->find('p');
            $body = '';
            $parts->each(function ($el) use (&$body) {
                $body .= $el->text."\n";
            });

            $image = $article->find('.article__main-image__image')[0];

            if ($image !== null) {
                $news->setImageUrl($image->getAttribute('src'));
            }

            $news->setTitle($htmlNode->find('.news-feed__item__title')[0]->text);
            $news->setBody($body);
            $news->setBodyShort($body);

            return $news;
        }

        return null;
    }
}